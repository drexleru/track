<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Make Track</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.8.2/ol.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">

		<style>		
			.popover-content {
	  			min-width: 150px;
	  			text-align: center
			}
		</style>
	</head>
	<body>
	<div class="container-fluid" style="margin-top: 10px">
		<div class="row" style="margin-bottom: 10px;">
			<div class="col-md-12">
				<form class="form-inline" action="#" role="form" id="add-courier" method="post">
					<div class="form-group">
						<label for="name">Nombre:</label> <br>
						<input type="text" name="name" id="name" class="form-control" required>				
					</div>

					<div class="form-group">
						<label for="nombre">Tipo:</label> <br>
						<select name="type" id="type" class="form-control" required="">
							<option value=""></option>
							<option value="moto">Moto</option>
							<option value="bici">Bici</option>
							<option value="van">Van</option>
						</select>				
					</div>

					<div class="form-group">

						<button type="submit" class="btn btn-success" style="margin-top: 25px">
							<i class="fa fa-plus"></i> 	Agregar
						</button>
					</div>				
				</form>
			</div>
		</div>
		<div class="row" style="margin-bottom: 10px;">
			<div class="col-md-12">
				<div id="map" style="width: 180vh; height: 80vh"></div>	
			</div>		
		</div>

		<div id="popup"></div>		
	</div>
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.8.2/ol.min.js"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="js/connect-socket.js"></script>
	<script src="js/helpers.js"></script>
	<script src="js/map.js"></script>
	<script>

		function sendAddCourier(courier){
			var send_data = {};
			send_data.entity = courier;
			send_data.action = 'added';
			send_data.func = 'viewAddedCourier';
			conn.send(JSON.stringify(send_data));
		}

		function viewAddedCourier(data){
			var courier = map.makeFeature(data);
			map.setInteraction(courier);
		}

		function sendTrackCourier(courier){
			var send_data = {};
			send_data.entity = courier;
			send_data.action = 'change location';
			send_data.func = 'viewChangedCourier';
			conn.send(JSON.stringify(send_data));
		}

		function viewChangedCourier(data){
			map.moveFeature(data);
		}

		var map = new Map('map', 'Bogotá');
		$('#add-courier').on('submit', function(e){
			e.preventDefault();
			var form = getFormData($(this));
			var courier = map.makeFeature(form, sendAddCourier);
			map.setInteraction(courier);
			map.makeTraking(courier, sendTrackCourier);
		});
	</script>	
	</body>
</html>