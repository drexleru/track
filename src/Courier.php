<?php 

namespace Track;
use Track\Model;

class Courier extends Model
{
	public $id;
	public $name;
	public $type;
	public $longitude;
	public $latitude;

	function __construct()
	{
		parent::__construct('couriers');
	}

	public function store(){
		$this->save($this);
	}
}
?>