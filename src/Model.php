<?php 
namespace Track;
use Track\FeedBack;
class Model
{
	private static $conn;
	private static $table;
	function __construct($table){
		self::$conn = \r\connect('localhost', 28015, 'track');	;		
		self::$table = $table;
	}

	protected function save($entity){
		$data = get_object_vars($entity);	
		$result = \r\table(self::$table)->insert($data)->run(self::$conn);
		$inserted = json_decode(json_encode($result))->inserted;
		if($inserted==1)
		{
			new FeedBack($data);
		}

	}

	protected function getAll() {
		return \r\table(self::$table)->run(self::$conn);
	}

	private function changeFeeds(){
		$feed = \r\table(self::$table)->changes()->run(self::$conn);
		foreach ($feed as $change) {
		  	print_r($change);
		}
	}

	protected function update()
	{

	}

	function __destruct(){
		self::$conn->close();
	}

}
