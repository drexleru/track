<?php 

namespace Track;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Tracking implements MessageComponentInterface {
    protected $couriers;

    public function __construct() {
        $this->couriers = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->couriers->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $data) {
        $numRecv = count($this->couriers) - 1;
        $cdata = json_decode($data);


        echo sprintf("Courier %s %s: lat[%s] - lon[%s] \n", 
                $cdata->entity->name, $cdata->action, 
                $cdata->entity->latitude, 
                $cdata->entity->longitude
            );
        
        foreach ($this->couriers as $courier) {
            if ($from !== $courier) {
                // The sender is not the receiver, send to each courier connected
                $courier->send($data);
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->couriers->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}