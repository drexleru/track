<?php

use Track\Courier;

require_once("rdb/rdb.php");
require dirname(__DIR__) . '/vendor/autoload.php';

if(isset($_POST['courier'])) {
	header('Content-type: application/json');
	$datos = $_POST['courier'];
	$courier = new Courier();
	$courier->id = $datos['id'];
	$courier->name = $datos['name'];
	$courier->type = $datos['type'];	
	$courier->longitude = $datos['longitude'];
	$courier->latitude = $datos['latitude'];
	$courier->store();
}




	
