function callFunctionTask(func, data){
	data = data || ''; 
	func = eval(func);
	func(data);
}


var conn = new WebSocket('ws://192.168.0.26:8000');
conn.onopen = function(e) {	
    console.log("Connection established!");
};

conn.onmessage = function(response) {

	var dataSocket = JSON.parse(response.data);

	callFunctionTask(dataSocket.func, {entity: dataSocket.entity}); 
};