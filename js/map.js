typeIcon = {
	'van':			
		new ol.style.Style({
			  	image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
				  	src: 'track/../img/icono-van.png'
					}))
			}),		

	'bici':
		new ol.style.Style({
		  		image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
				    src: 'track/../img/icono-bicicleta.png'
		  		}))
			}),

	'moto':
		new ol.style.Style({
					image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({

				    src: 'track/../img/icono-casco.png',
				   	anchor:[0.5, 0.5],
					}))
			})
	};



function Map(target, city)
{
	var Map = this;
	var element;
	var selectfeature;
	var centers = {
		'Bogotá': {coordinates: [-74.095447, 4.656168], zoom: 10},
		'Cali': {coordinates: [-76.521133595847, 3.4299578948874], zoom: 13},
		'Barranquilla': {coordinates: [-74.784727103572, 10.980610723662], zoom: 13},
	};


	Map.vectorSource = new ol.source.Vector({});

	Map.vectorLayer = new ol.layer.Vector({});

	Map.map = new ol.Map({
	    target: target,
	    layers: [
	        new ol.layer.Tile({
	            source: new ol.source.MapQuest({layer: 'osm'})
	        }),


	    ],
	    view: new ol.View({
	        center: ol.proj.fromLonLat(centers[city].coordinates),  
	        zoom: centers[city].zoom,
	        minZoom: 12
	    })
	});


	var popup_show = false;

    var popup = new ol.Overlay({
		element: document.getElementById('popup')
	});


	var showPopupClick = function(){
		Map.map.addOverlay(popup);

		Map.map.on('click', function(evt) {
			element = popup.getElement();
			var coordinate = evt.coordinate;
			console.log('event c');
			console.log(coordinate);
			var feature = Map.map.forEachFeatureAtPixel(evt.pixel,
			  function(feature, layer) {
			  	selectfeature = feature.getId();
			    return feature;
			});

			showPopover(feature, coordinate);
		});
	}


	var showPopover = function(feature, coordinate){
		if(feature){

		  $(element).popover('destroy');
		  popup_show = true;
		  popup.setPosition(coordinate);
		  // the keys are quoted to prevent renaming in ADVANCED mode.
		  $(element).popover({
		      'placement': 'top',
		      'animation': false,
		      'html': true,
		      'content': feature.get('entity').name
		  });
		  $(element).popover('show');
		} else {
		  $(element).popover('destroy');
		  popup.setPosition(undefined);
		  popup_show = false;
		}
	}


	Map.makeFeature = function(properties, callback) 
	{
		callback = callback ||  null;
		console.log(properties);
		var feature = new ol.Feature({
			  	population: 4000,
			  	rainfall: 500
			});

		if(typeof properties.entity == "undefined")
		{
			var coor = new ol.geom.Point(ol.proj.transform(
				  				[
				  					parseFloat(centers[city].coordinates[0]),
				  					parseFloat(centers[city].coordinates[1])
				  				], 'EPSG:4326', 'EPSG:3857'));
			
			feature.setGeometry(coor);
			var location = ol.proj.transform(feature.getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326');
			
			properties.id =  '_' + Math.random().toString(36).substr(2, 9);
			properties.longitude = location[0];
			properties.latitude = location[1];
			feature.set('entity', properties);
			feature.setStyle(typeIcon[properties.type]);
		}else{
			console.log('ingresa por entity');
			feature.setGeometry(
				new ol.geom.Point(ol.proj.transform(
				  				[
				  					parseFloat(properties.entity.longitude),
				  					parseFloat(properties.entity.latitude)
				  				], 'EPSG:4326', 'EPSG:3857'))
			);
			feature.setId(properties.entity.id);

			feature.set('entity', properties.entity);
			feature.setStyle(typeIcon[properties.entity.type]);

		}


		Map.vectorSource.addFeature(feature);
		Map.vectorLayer.setSource(Map.vectorSource);
		Map.map.removeLayer(Map.vectorSource);
		Map.map.addLayer(Map.vectorLayer);

		popup_show = false;

		if(callback!=null)
			callback(feature.get('entity'));

		return feature;
	};

	Map.setInteraction = function(feature){
		console.log(ol.proj.transform(feature.getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326'));
		var dragInteraction = new ol.interaction.Modify({
			features: new ol.Collection([feature]),
    		style: null,
    		pixelTolerance: 18
		});

		Map.map.addInteraction(dragInteraction);
	};

	Map.makeTraking = function(feature, callback){
		var courier = {};
	    feature.on('change',function(){
	    	var coor = ol.proj.transform(this.getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326');
	    	courier.lon = coor[0];
	    	courier.lat = coor[1];
	    	courier.id = feature.get('entity').id;
	    	callback(courier);
		},feature);

		
	}

	Map.moveFeature = function(data) {
		console.log('moving');
		console.warn(data);
		var move = data.entity;
		Map.vectorSource.forEachFeature(function(feature){
			if(feature.getId() == move.id)
			{
				console.log('movido mk');

				var coor = new ol.geom.Point(ol.proj.transform([parseFloat(move.lon),	parseFloat(move.lat)], 'EPSG:4326', 'EPSG:3857'));
				if(popup_show==true && selectfeature == feature.getId())				{
					
	    			showPopover(feature, feature.getGeometry().getCoordinates());
				}
			    feature.setGeometry(coor);
				
			}
		});
	}

	showPopupClick();
}